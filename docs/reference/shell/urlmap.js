baseURLs = [
    [ 'GLib', 'https://docs.gtk.org/glib/' ],
    [ 'GObject', 'https://docs.gtk.org/gobject/' ],
    [ 'Gio', 'https://docs.gtk.org/gio/' ],
    [ 'GdkPixbuf', 'https://docs.gtk.org/gdk-pixbuf/' ],
    [ 'Meta', 'https://gnome.pages.gitlab.gnome.org/mutter/meta/' ],
    [ 'Mtk', 'https://gnome.pages.gitlab.gnome.org/mutter/mtk/' ],
    [ 'Cogl', 'https://gnome.pages.gitlab.gnome.org/mutter/cogl/' ],
    [ 'Clutter', 'https://gnome.pages.gitlab.gnome.org/mutter/clutter/' ],
    [ 'St', 'https://gnome.pages.gitlab.gnome.org/gnome-shell/st/' ],
]
